package jet

import (
	"net/http"
	"runtime"
	"time"

	humanize "github.com/dustin/go-humanize"
)

type CoreService struct {
	RestService `root:"/jet/" prefix:"-"`
	ping        GET `url:"/ping"`
	status      GET `url:"/status" pretty:"true"`
	date        GET `url:"/time"`
}

func (me *CoreService) Ping() string {
	return "pong"
}

func (me *CoreService) Status() map[string]interface{} {

	out := make(map[string]interface{})

	m := runtime.MemStats{}
	runtime.ReadMemStats(&m)

	mem := make(map[string]interface{})

	mem_gen := make(map[string]interface{})
	mem_gen["alloc"] = humanize.Bytes(m.Alloc)
	mem_gen["total_alloc"] = humanize.Bytes(m.TotalAlloc)
	mem["general"] = mem_gen

	mem_hp := make(map[string]interface{})
	mem_hp["alloc"] = humanize.Bytes(m.HeapAlloc)
	mem_hp["sys"] = humanize.Bytes(m.HeapAlloc)
	mem_hp["idle"] = humanize.Bytes(m.HeapIdle)
	mem_hp["inuse"] = humanize.Bytes(m.HeapInuse)
	mem_hp["released"] = humanize.Bytes(m.HeapReleased)
	mem_hp["objects"] = humanize.Bytes(m.HeapObjects)
	mem["heap"] = mem_hp

	out["mem"] = mem
	out["server-time"] = time.Now().Format("2006-01-02 15:04:05 MST")
	out["go-version"] = runtime.Version()[2:]
	out["jet-version"] = release

	return out
}

func (me *CoreService) Date(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(time.Now().Format("2006-01-02 15:04:05 MST")))
}
