package jet

import (
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
	"time"
)

func TestCoreFunctions(t *testing.T) {

	s := NewRestServer()
	port := getUniquePortForTestCase()
	s.Port = port
	s.RunAsync()

	Convey("When you start a RestServer", t, func() {

		Convey("Then /jet/ping should response with pong", func() {
			url := fmt.Sprintf("http://localhost:%d/jet/ping", port)
			code, ctype, content := getUrl(url, nil)
			So(code, ShouldEqual, 200)
			So(ctype, ShouldEqual, "text/plain")
			So(content, ShouldEqual, "pong")
		})

		Convey("Then /jet/status should response with a json object", func() {
			url := fmt.Sprintf("http://localhost:%d/jet/status", port)
			code, ctype, content := getUrl(url, nil)
			So(code, ShouldEqual, 200)
			So(ctype, ShouldEqual, "application/json")
			So(content, ShouldContainSubstring, "jet-version")
			So(content, ShouldContainSubstring, "server-time")
		})

		Convey("Then /jet/time should respond with a timestamp", func() {
			url := fmt.Sprintf("http://localhost:%d/jet/time", port)
			code, ctype, content := getUrl(url, nil)
			So(code, ShouldEqual, 200)
			So(ctype, ShouldStartWith, "text/plain")
			_, err := time.Parse("2006-01-02 15:04:05 MST", content)
			So(err, ShouldBeNil)
		})
	})

}
