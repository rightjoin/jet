package jet

import (
	"fmt"
	"testing"

	"net/http"
	"strings"

	. "github.com/smartystreets/goconvey/convey"
)

type aideService struct {
	RestService
	echo  GET
	echo2 GET
	json  POST
}

func (u *aideService) Echo(j Aide) string {
	j.LoadVars()
	return j.Query["abc"]
}

func (u *aideService) Echo2(j Aide) string {
	return j.Query["def"]
}

func (u *aideService) Json(j Aide) string {
	j.LoadVars()
	return j.Post["info"]
}

func TestJarForHttpGETMethod(t *testing.T) {

	s := NewRestServer()
	s.AddService(&aideService{})
	s.Port = getUniquePortForTestCase()
	s.RunAsync()

	Convey("Given a RestServer and a service", t, func() {
		Convey("Echo service should return Query String assigned to key: abc", func() {
			url := fmt.Sprintf("http://localhost:%d/aide/echo?abc=whatsUp", s.Port)
			_, _, content := getUrl(url, nil)
			So(content, ShouldEqual, "whatsUp")
		})
		Convey("Echo2 service should fail since LoadVars is not invoked", func() {
			url := fmt.Sprintf("http://localhost:%d/aide/echo2?def=hello", s.Port)
			_, _, content := getUrl(url, nil)
			So(content, ShouldEqual, "")
		})

	})
}

func TestJsonVariables(t *testing.T) {
	s := NewRestServer()
	s.AddService(&aideService{})
	s.Port = getUniquePortForTestCase()
	s.RunAsync()

	url := fmt.Sprintf("http://localhost:%d/aide/json", s.Port)

	Convey("Given a RestServer with a service", t, func() {
		Convey("info should expect a string", func() {
			posty, _ := http.NewRequest("POST", url, strings.NewReader(`{"info":"a-string"}`))
			posty.Header.Set("Content-Type", "application/json")
			_, _, data := processRequest(posty)
			So(data, ShouldEqual, `a-string`)
		})
		Convey("info should expect a int", func() {
			posty, _ := http.NewRequest("POST", url, strings.NewReader(`{"info":12345}`))
			posty.Header.Set("Content-Type", "application/json")
			_, _, data := processRequest(posty)
			So(data, ShouldEqual, `12345`)
		})
		Convey("info should expect a array", func() {
			posty, _ := http.NewRequest("POST", url, strings.NewReader(`{"info":[123, "abc"]}`))
			posty.Header.Set("Content-Type", "application/json")
			_, _, data := processRequest(posty)
			So(data, ShouldEqual, `[123,"abc"]`)
		})
	})
}
