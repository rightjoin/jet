package jet

import (
	"fmt"
	"strconv"
)

type Fault struct {
	HTTPCode int    `json:"-"`
	EIN      int    `json:"ein"`
	Message  string `json:"message"`
	Issue    error  `json:"issue"`
}

func (f Fault) MarshalJSON() ([]byte, error) {

	// TODO: use buffer, and not immutable strings

	b := "{"

	b += fmt.Sprintf(`"ein":%d,`, f.EIN)
	b += fmt.Sprintf(`"message":%s`, strconv.Quote(f.Message))
	if f.Issue != nil {
		b += fmt.Sprintf(`,"issue": %s`, strconv.Quote(f.Issue.Error()))
	}
	b += "}"

	return []byte(b), nil
}

func (f Fault) Set(err error) error {
	if err == nil {
		return nil
	}

	return Fault{Message: f.Message, EIN: f.EIN, HTTPCode: f.HTTPCode, Issue: err}
}

// Fault implements error interface
func (f Fault) Error() string {
	if f.Issue == nil {
		return ""
	}
	return f.Issue.Error()
}
