package jet

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/rightjoin/ion"

	log "github.com/rightjoin/log15"
)

var separator string = ","

// Aide allows access to Request, Response and other vars (post, get, body)
type Aide struct {
	// request handle
	Request  *http.Request
	Response http.ResponseWriter

	// variables extracted from httpRequest
	Post   Mapped
	Query  Mapped
	Header Mapped
	Body   string

	// application-specific configs
	Vars ion.M
}

// NewAide creates a new Aide object
func NewAide(w http.ResponseWriter, r *http.Request, v map[string]interface{}) Aide {
	if v == nil {
		// guarantee that v is not null
		v = ion.M{}
	}
	return Aide{
		Request:  r,
		Response: w,
		Vars:     v,
	}
}

// LoadVars parses an intializes PostVars, GetVars and Body variables
func (j *Aide) LoadVars() {

	if j.Post != nil {
		// assume that user is calling it again accidentally
		// and the request has already been parsed
		return
	}

	j.Post = make(Mapped)
	j.Query = make(Mapped)
	j.Header = make(Mapped)

	// Load header
	for key, val := range j.Request.Header {
		j.Header[key] = strings.Join(val, separator)
	}

	// Load em up
	if j.Request.Method == "GET" { // GET => Query only
		j.Request.ParseForm()
		j.loadQueryVar(j.Request, false)
	} else { // POST, PUT, DELETE => Query and Post both
		ctype := j.Request.Header.Get("Content-Type")
		switch {
		case strings.HasPrefix(ctype, "application/x-www-form-urlencoded"):
			j.Request.ParseForm()
			j.loadPostVar(j.Request)
			j.loadQueryVar(j.Request, true)
		case strings.HasPrefix(ctype, "multipart/form-data;"):
			// ParseMultiPart form should ideally populate
			// r.PostForm, but instead it fills r.Form
			// https://github.com/golang/go/issues/9305
			j.Request.ParseMultipartForm(1024 * 1024)
			j.loadPostVar(j.Request)
			j.loadQueryVar(j.Request, true)
		case ctype == "application/json":
			j.Body = string(getBody(j.Request))
			j.loadQueryVar(j.Request, false)
			j.loadJsonVar(j.Request)
		default:
			j.Body = string(getBody(j.Request))
		}
	}
}

func getBody(r *http.Request) []byte {
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	defer r.Body.Close()
	return b
}

func (j *Aide) loadPostVar(r *http.Request) {
	for k := range r.PostForm {
		j.Post[k] = strings.Join(r.PostForm[k], separator)
	}
}

func (j *Aide) loadQueryVar(r *http.Request, skipPostVars bool) {
	for k := range r.Form {
		if skipPostVars {
			// only add to query-vars if it is NOT a post var
			if _, found := j.Post[k]; !found {
				j.Query[k] = strings.Join(r.Form[k], separator)
			}
		} else {
			j.Query[k] = strings.Join(r.Form[k], separator)
		}
	}

}

func (j *Aide) loadJsonVar(r *http.Request) {
	jsn := make(map[string]interface{})
	err := json.Unmarshal([]byte(j.Body), &jsn)
	if err != nil {
		log.Error("json parse error", "ref", "jet", "err", err)
		return
	}
	for key, dat := range jsn {
		if val, ok := dat.(string); ok {
			j.Post[key] = val
		} else {
			byt, err := json.Marshal(dat)
			if err != nil {
				log.Error("json marshal error", "ref", "jet", "err", err)
				return
			}
			j.Post[key] = string(byt)
		}
	}
}

type Mapped map[string]string

func (m Mapped) Int(key string, dflt int) int {
	if s, ok := m[key]; ok {
		if i, err := strconv.ParseInt(s, 10, 32); err == nil {
			return int(i)
		}
	}
	return dflt
}

func (m Mapped) IntMust(key string) int {
	s, ok := m[key]
	if !ok {
		panic("key not found in map: " + key)
	}

	i, err := strconv.ParseInt(s, 10, 32)
	if err != nil {
		fmt.Errorf("cant parse uint: %s", err)
	}

	return int(i)
}

func (m Mapped) String(key string, dflt string) string {
	if s, ok := m[key]; ok {
		return s
	}
	return dflt
}

func (m Mapped) UintMust(key string) uint {
	s, ok := m[key]
	if !ok {
		panic("key not found in map: " + key)
	}

	u64, err := strconv.ParseUint(s, 10, 32)
	if err != nil {
		fmt.Errorf("cant parse uint: %s", err)
	}

	return uint(u64)
}

func (m Mapped) HasKey(key string) bool {
	_, ok := m[key]
	return ok
}

func (m Mapped) HasKeys(keys ...string) error {
	var missing string
	for _, k := range keys {
		if !m.HasKey((k)) {
			missing += k + ","
		}
	}
	if missing != "" {
		return errors.New(strings.TrimRight(missing, ",") + " missing")
	} else {
		return nil
	}
}
