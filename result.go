package jet

type Result struct {
	Success int                    `json:"success"`
	Data    map[string]interface{} `json:"data,omitempty"`
}
